const express = require("express");
const fs = require("fs");

const cors = require("cors");

const app = express();
//caminho da imagem
const path = require("path")

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());
//chamar imagem
app.use(express.static(path.join(__dirname, "public")));

const porta = 3001;

app.listen(porta, function () {
    console.log(`Servidor rodando na porta ${porta}`);
});


app.get("/cargainicial", function (req, resp) {
    //Efetuando a leitura do arquivo
    fs.readFile("config.csv", "utf8", function (err, data) {
        //Enviando para o console o resultado da leitura

        if (err) {
            console.log("Erro ao ler arquivo: " + err);
        } else {
        }

        console.log(data);

        var dados = data.split("\n");
        console.log(dados);

        let candidatos = [];

        for (let i = 0; i < dados.length; i++) {
            const element = dados[i];
            candidatos.push(element.split(","));
        }
        //console.log(candidatos);

        resp.send(candidatos);
    });
});

//enviando voto para o config.csv
app.post("/voto", function (req, resp) {
    console.log(req.body.voto);

    let voto = `${req.body.rg},${req.body.numerocandidato},${req.body.date}`;

    fs.appendFile("votacao.csv", `${voto},\n`, function (err) {
        if (err) {
            resp.json({
                Status: "500",
                mensagem: "Erro ao registrar voto, contate o administrador do sistema",
            });
        } else {
            resp.json({
                Status: "200",
                mensagem: "Voto Registrado Com sucesso",
            });
        }
    });
});

//apuracao de votos
app.get("/apuracao", function (req, resp) {
    fs.readFile("votacao.csv", "utf8", function (err, data) {
        if (err) {
            throw err; //console.log("Erro ao ler arquivo: " + err);
        } else {
            var dados = data.split("\n");

            var votosCandidatos = [];

            dados.forEach((element) => {
                votosCandidatos.push(element.split(","));
            });

            fs.readFile("config.csv", "utf8", function (err, data) {
                var dados = data.split("\n");

                var candidatos = [];
                dados.forEach((element) => {
                    candidatos.push(element.split(","));
                });

                let apuracao = [];

                candidatos.forEach((candidato) => {
                    votosContabilizados = 0;
                    votosCandidatos.forEach((voto) => {
                        if (voto[1] == candidato[1]) {
                            votosContabilizados++;
                        }
                    });
                    apuracao.push([
                        candidato[1],
                        candidato[2],
                        candidato[3],
                        votosContabilizados,
                    ]);
                });

                //ordenar dados e deixar em ordem decrescente
                apuracao.sort((a, b) => {
                    return a[3] - b[3];
                });
                apuracao.reverse();
                console.log(apuracao);
                resp.send(apuracao);
            });
        }
    });
});


//apuracao de votos
app.get("/apuracaoIdenticada", function (req, resp) {
    fs.readFile("votacao.csv", "utf8", function (err, data) {
        if (err) {
            throw err; //console.log("Erro ao ler arquivo: " + err);
        } else {
            var dados = data.split("\n");

            var votosCandidatos = [];

            dados.forEach((element) => {
                votosCandidatos.push(element.split(","));
            });

            //ordenando matriz pelo rg (para que o primeiro voto seja o q vale)
            votosCandidatos.sort((a, b) => {
                return a[0] - b[0]
            })

            console.log(votosCandidatos);

            //colocando o for em um while para q ele compare até estarem todos diferentes (por causa do length - 1)

            var entrou = true
            while (entrou) {
                entrou = false
                for (let i = 0; i < (votosCandidatos.length - 1); i++) {
                    if (votosCandidatos[i][0] === votosCandidatos[i + 1][0]) {
                        votosCandidatos.splice((i + 1), 1)
                        entrou = true
                    }

                }

            }
            console.log(votosCandidatos);

            fs.readFile("config.csv", "utf8", function (err, data) {
                var dados = data.split("\n");

                var candidatos = [];
                dados.forEach((element) => {
                    candidatos.push(element.split(","));
                });




                let apuracao = [];

                candidatos.forEach((candidato) => {
                    votosContabilizados = 0;
                    votosCandidatos.forEach((voto) => {
                        if (voto[1] == candidato[1]) {
                            votosContabilizados++;
                        }
                    });
                    apuracao.push([
                        candidato[1],
                        candidato[2],
                        candidato[3],
                        votosContabilizados,
                    ]);
                });

                //ordenar dados e deixar em ordem decrescente
                apuracao.sort((a, b) => {
                    return a[3] - b[3];
                });
                apuracao.reverse();
                console.log(apuracao);
                resp.send(apuracao);
            });
        }
    });
});
