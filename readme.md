PROJETO URNA ELETRONICA COM NODE EXPRESS

01 . criacao
=====================================================================================================================
1. Criar um projeto NODE com express para mapear algumas rotas para nosso sistema 
    de eleiçoes. 

    [x] npm init
    [x] npm install express
    [x] server.js
    [x] criar rota index.html
    [x] testar rota 

2. Criar um arquivo config.csv no backende/servidor com o seguinte formato: (tipoEleicao,numeroCandidato,nomeCandidato,urlFoto) 
     exemplo de conteúdo para o arquivo csv: 
    a,10,felisberto,img/foto.jpg
    a,11,alberto,img/foto2.jpg
    a,12,alberto,img/foto2.jpg

    [x] criar config.csv
    [x] editar formato
    [x] testar 


3. No backende criar um endpoint/rota ([get]/cargainicial) para fornecer os dados doa rquivo config.csv.  Esses dados devem ser fornecidos no corpo da resposta/response no formato array.  
    
    [x] criar endpoint
   

   4. No Frontend(urna eletrônica) implementar uma função para fazer a requisição dos dados de "configuração"/carga inicial. 
   Chamar a função ao carregar a urna eletrônica (ao ligar a urna eletrônica) ---> isso vai retornar um array.
   Usar os dados recebidos para carregar/configurar a urna eletrônica:
     - se na posição [0,0] do array config contiver "a", então ocultar o campo RG da tela; 
     - ler as posições 1, 2, 3 e carregar nas estruturas que serão exibidas na tela --> significa refatorar o frontend 
     
     [x] adicionar input rg
     [x] criar funcao cargainicial()
     [x] ler a posicao [0,0] 


     02. Registro de Votos 
=======================================================================================================
1. Crie um novo endpoint/rota ([POST]/voto) para registrar/salvar os votos. 
   Este endpoind deve receber os dados do formulário no corpo da requisição. 
   Cada novo voto deverá ser gravado em um arquivo chamado votacao.csv com o seguinte formato: (RG,numeroCandidato,timestampVoto)
   
   [x] criar rota post voto
   [x] receber dados na rota post voto

   Exemplo de conteúdo para o arquivo csv: 

       4072676,,202109022123219999
       4072676,11,202109022123219999

       OBS. Em caso de votação anônima, deixar o RG vazio: 

       ,11,202109022123219999
       ,11,202109022123219999
       
   [x] criar estrutua if (){
       Caso o voto seja registrado/gravado com sucesso, responder com o seguinte json: 

           "Status" : "200"
           mensagem : "Voto Registrado Com sucesso"
        

       } else{

   [x]  Caso ocorra algum erro ao registrar o voto, responder com o seguinte json: 
        
           "Status" : "500"
           mensagem : "Erro ao registrar voto, contate o administrador do sistema"
         
       }



2. No Frontend, implemente uma função que faça requisições para o novo endpoind /voto. 
   
   A cada novo voto chamar a função passando os dados da votação. 
   Exibir na tela a mensagem retornada pela chamada do Endpoint. 
      Caso o Status seja 200, exibir a mensagem por 2 seguntos.
      Caso a mensagem seja 500, exibir a mensagem por tempo indeterminado e deixar a mensagem em destaque (em vermelho, por exemplo)
      Desabilitar os botes e campos da tela durante o tempo em que a mensagem estiver sendo exibida.



FN03. Apuração dos votos
=======================================================================================================
1. Criar um novo endpoint/rota ([get]/apuracao) para retornar a apuração dos votos. 
   Ler o arquivo votacao.csv e contabilizar a quantidade de votos de cada candidato, votos brancos e votos nulos
   Ao final da apuração retornar no response um array com o seguinte formato: 
       [numeroDoCanditato,nomeCandidato,urlFotoCandidato,qtdVotos]  
       onde: numeroCandidato pode assumir o número do candidato, ou os termos ("Brancos" ou "Nulos")

       Obs. O array deve estar ordenado em ordem decrescente pela quantidade de votos

       [x] endpoint rota get (apuracao)
       [x] ler arquivos votacao.csv
       [x] contabilizar votos
       [x] criacao do array
       [x] ordenadar array em ordem decrescente
      
2. Criar um novo Projeto Frontend para apresentar a apuração da votação.
   ** O layout/design desta página HTML pode ser definido livremente pelo desenvolvedor 
   [x] criar novo html
   [x] tabela dinamica que recebe os dados da apuracao do voto



[x]3. No backend adicionar  os caminhos
  
            app.use(express.static(path.join(__dirname, "public")));
             const path = require("path")

FN04. Apuracao de votos em votacao identificada
==========================================================
[x]1. criar um novo endpoint(get[apuracaoIdenticada]), trazer os resultados da FN03, porem desprezando votos duplicados(considerar apenas um voto)
*São considerados votos duplicados aqueles que contem o mesmo RG

2. FRONTEND adicionar um radio button para selecionar o tipo de apuracao
  
   